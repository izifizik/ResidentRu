module resident_ru

go 1.16

require (
	github.com/gin-gonic/gin v1.8.1
	github.com/jackc/pgx/v4 v4.17.2
	github.com/rs/zerolog v1.28.0
)

require (
	github.com/gin-gonic/contrib v0.0.0-20221130124618-7e01895a63f2
	golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa
)
