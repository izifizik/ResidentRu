package main

import (
	"github.com/rs/zerolog/log"
	"resident_ru/internal/app"
)

func main() {
	err := app.Run()
	if err != nil {
		log.Error().Err(err).Send()
		return
	}
}
