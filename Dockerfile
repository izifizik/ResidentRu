FROM golang:1.16 as builder
COPY go.mod /go/api/
COPY go.sum /go/api/
WORKDIR /go/api
RUN go mod tidy
COPY . /go/api
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o ./ cmd/main.go

FROM alpine:latest
COPY --from=builder /main ./
ENV enviroment=production
RUN chmod +x ./main
ENTRYPOINT ["./main"]

EXPOSE 8000