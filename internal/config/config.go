package config

import (
	"os"
)

type Config struct {
	App AppConfig
	DB  DBConfig
}

type AppConfig struct {
	Port     string
	LogLevel string
}

type DBConfig struct {
	DBString string
}

func ReadConfig() (*Config, error) {
	var config Config
	var err error
	//app parse

	config.App.Port = os.Getenv("SERVER_PORT")
	if config.App.Port == "" {
		config.App.Port = "8080"
	}

	//db parse
	config.DB.DBString = os.Getenv("DBSTRING")
	//if config.DB.DBString == "" {
	//	return nil, errors.New("not specified DBSTRING")
	//}

	return &config, err

}
