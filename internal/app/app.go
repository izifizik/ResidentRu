package app

import (
	"github.com/rs/zerolog"
	"os"
	"resident_ru/internal/config"
	"resident_ru/internal/controller"
	"resident_ru/internal/logic"
	"resident_ru/internal/repo/postgres"
)

func Run() error {
	log := zerolog.New(os.Stdout).With().Timestamp().Logger()

	log.Info().Msg("Start read config")
	cfg, err := config.ReadConfig()
	if err != nil {
		return err
	}
	log.Info().Msg("Finish read config")

	log.Info().Msg("Start database connect")
	postgresPool, err := postgresql.CreateDatabasePoolConnections(&cfg.DB)
	if err != nil {
		return err
	}
	defer postgresPool.Close()
	log.Info().Msg("Finish database connect")

	repo := postgresql.NewRepo(postgresPool)
	lgc := logic.NewLogic(cfg, repo)
	api := controller.NewApp(cfg, lgc)

	api.StartServe()

	return nil
}
