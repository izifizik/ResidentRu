package postgresql

import (
	"context"
	"github.com/jackc/pgx/v4"
	"resident_ru/internal/domain"
)

func (r *Repo) GetNews(ctx context.Context, title string) ([]domain.News, error) {
	q := `
select id, title, description, created_at from news 
                                               where ($1='' or title ilike '%' || lower($1) || '%') 
                                               order by created_at desc`

	rows, err := r.DB.Query(ctx, q, title)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	return rowsToNews(rows)
}
func rowsToNews(rows pgx.Rows) ([]domain.News, error) {
	var n []domain.News

	for rows.Next() {
		var v domain.News
		err := rows.Scan(&v.ID, &v.Title, &v.Description, &v.CreatedAt)
		if err != nil {
			return nil, err
		}
		n = append(n, v)
	}
	return n, nil
}

func (r *Repo) GetNewsByID(ctx context.Context, nID int64) (domain.News, error) {
	q := `
select id, title, description, created_at from news where id = $1`
	var news domain.News
	err := r.DB.QueryRow(ctx, q, nID).Scan(&news.ID, &news.Title, &news.Description, &news.CreatedAt)
	return news, err
}

func (r *Repo) CreateNews(ctx context.Context, news domain.NewsDTO) (int64, error) {
	q := `
insert into news (title, description) values ($1, $2) returning id`
	var id int64
	err := r.DB.QueryRow(ctx, q, news.Title, news.Description).Scan(&id)
	return id, err
}

func (r *Repo) UpdateNews(ctx context.Context, news domain.NewsDTO, nID int64) error {
	q := `
update news set title = coalesce($1, title), 
                description = coalesce($2, description)
            where id = $3`
	_, err := r.DB.Exec(ctx, q, news.Title, news.Description, nID)
	return err
}

func (r *Repo) DeleteNews(ctx context.Context, nID int64) error {
	q := `
delete from news where id = $1`
	_, err := r.DB.Exec(ctx, q, nID)
	return err
}
