package postgresql

import (
	"context"
	"github.com/jackc/pgx/v4"
	"resident_ru/internal/domain"
)

func (r *Repo) GetReports(ctx context.Context, uID int64, isAdmin bool, title string) ([]domain.Reports, error) {
	if isAdmin {
		qAdmin := `
select r.id, title, description, created_at from reports r 
        where ($1='' or title ilike '%' || lower($1) || '%')`
		rows, err := r.DB.Query(ctx, qAdmin, title)
		if err != nil {
			return nil, err
		}
		defer rows.Close()
		return rowsToReports(rows)
	}
	q := `select r.id, title, description, created_at from reports r 
                                               where r.user_id = $1 and ($2='' or title ilike '%' || lower($2) || '%')`
	rows, err := r.DB.Query(ctx, q, uID, title)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	return rowsToReports(rows)
}
func rowsToReports(rows pgx.Rows) ([]domain.Reports, error) {
	var n []domain.Reports

	for rows.Next() {
		var v domain.Reports
		err := rows.Scan(&v.ID, &v.Title, &v.Description, &v.CreatedAt)
		if err != nil {
			return nil, err
		}
		n = append(n, v)
	}
	return n, nil
}

func (r *Repo) GetReportByID(ctx context.Context, rID int64) (domain.ReportedMessages, error) {
	q := `
select title, description from reports where id = $1`
	var rm domain.ReportedMessages
	err := r.DB.QueryRow(ctx, q, rID).Scan(&rm.Title, &rm.Description)
	if err != nil {
		return domain.ReportedMessages{}, err
	}

	q = `
select rm.id, message, user_id, u.first_name, u.last_name from reported_messages rm left join users u on u.id = rm.user_id 
                                 where report_id = $1 order by id`
	rows, err := r.DB.Query(ctx, q, rID)
	if err != nil {
		return domain.ReportedMessages{}, err
	}
	rm.Messages, err = rowsToReportedMessages(rows)

	return rm, err
}
func rowsToReportedMessages(rows pgx.Rows) ([]domain.Messages, error) {
	n := make([]domain.Messages, 0)

	for rows.Next() {
		var v domain.Messages
		err := rows.Scan(&v.ID, &v.Message, &v.UserID, &v.UserFirstName, &v.UserLastName)
		if err != nil {
			return nil, err
		}
		n = append(n, v)
	}
	return n, nil
}

func (r *Repo) CreateMessage(ctx context.Context, message string, rID, uID int64) error {
	q := `insert into reported_messages (report_id, user_id, "message") values ($1, $2, $3)`

	_, err := r.DB.Exec(ctx, q, rID, uID, message)
	return err
}

func (r *Repo) EditMessage(ctx context.Context, m domain.MessageDTO, mID int64) error {
	q := `
update reported_messages set message = $1 where id = $2`
	_, err := r.DB.Exec(ctx, q, m.Message, mID)
	return err
}

func (r *Repo) DeleteMessage(ctx context.Context, mID int64) error {
	q := `
delete from reported_messages where id = $1`
	_, err := r.DB.Exec(ctx, q, mID)
	return err
}

func (r *Repo) CreateReport(ctx context.Context, report domain.ReportDTO, uID int64) (int64, error) {
	q := `
insert into reports (title, description, user_id, created_at) values ($1, $2, $3, now()) returning id`
	var id int64
	err := r.DB.QueryRow(ctx, q, report.Title, report.Description, uID).Scan(&id)
	return id, err
}

func (r *Repo) UpdateReport(ctx context.Context, report domain.ReportDTO, rID int64) error {
	q := `
update reports set title = coalesce($1, title), 
                description = coalesce($2, description) 
            where id = $3`
	_, err := r.DB.Exec(ctx, q, report.Title, report.Description, rID)
	return err
}

func (r *Repo) DeleteReport(ctx context.Context, rID int64) error {
	q := `
delete from reported_messages where report_id = $1`
	_, err := r.DB.Exec(ctx, q, rID)
	if err != nil {
		return err
	}
	q = `
delete from reports where id = $1`
	_, err = r.DB.Exec(ctx, q, rID)
	return err
}
