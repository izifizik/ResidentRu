package postgresql

import (
	"context"
	"math/rand"
	"resident_ru/internal/domain"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func (r *Repo) GetUserByUserID(ctx context.Context, uID int64) (domain.User, error) {
	q := `select id, "login", phone, room, last_name, first_name, is_admin from users where id = $1`

	var u domain.User
	err := r.DB.QueryRow(ctx, q, uID).Scan(&u.ID, &u.Login, &u.Phone, &u.Room, &u.LastName, &u.FirstName, &u.IsAdmin)
	return u, err
}

func (r *Repo) GetUserByLogin(ctx context.Context, login string) (domain.User, error) {
	q := `select id, login, phone, room, last_name, first_name, is_admin from users where login = $1`

	var u domain.User
	err := r.DB.QueryRow(ctx, q, login).Scan(&u.ID, &u.Login, &u.Phone, &u.Room, &u.LastName, &u.FirstName, &u.IsAdmin)
	return u, err
}

func (r *Repo) GetHashPasswordByLogin(ctx context.Context, login string) (string, error) {
	q := `select password from users where login = $1`
	var pass string
	err := r.DB.QueryRow(ctx, q, login).Scan(&pass)
	return pass, err
}

func (r *Repo) CreateUser(ctx context.Context, a domain.AuthDTO) (int64, error) {
	q := `insert into users ("password", "login", phone, room, last_name, first_name) values ($1, $2, $3, $4, 'МВП', 'ЮЗЕР') returning id`
	phone := rand.Intn(799999999999-711111111111) + 711111111111
	room := rand.Intn(999-100) + 100
	var id int64
	err := r.DB.QueryRow(ctx, q, a.Password, a.Login, phone, room).Scan(&id)
	return id, err
}
