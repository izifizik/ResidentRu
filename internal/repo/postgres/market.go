package postgresql

import (
	"context"
	"github.com/jackc/pgx/v4"
	"resident_ru/internal/domain"
)

func (r *Repo) GetMarkets(ctx context.Context, title string) ([]domain.Market, error) {
	q := `
select market.id, title, description, created_at, u.room, u.phone, u.first_name, u.last_name from market 
                                          left join users u on u.id = market.user_id
                                               where ($1='' or title ilike '%' || lower($1) || '%') 
                                               order by created_at desc`

	rows, err := r.DB.Query(ctx, q, title)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	return rowsToMarkets(rows)
}
func rowsToMarkets(rows pgx.Rows) ([]domain.Market, error) {
	var n []domain.Market

	for rows.Next() {
		var v domain.Market
		err := rows.Scan(&v.ID, &v.Title, &v.Description, &v.CreatedAt, &v.OwnersRoom,
			&v.OwnersPhone, &v.OwnerFirstName, &v.OwnerLastName)
		if err != nil {
			return nil, err
		}
		n = append(n, v)
	}
	return n, nil
}

func (r *Repo) GetMarketByID(ctx context.Context, nID int64) (domain.Market, error) {
	q := `
select market.id, title, description, created_at, u.room, u.phone, u.first_name, u.last_name from market 
                                          left join users u on u.id = market.user_id where market.id = $1`
	var market domain.Market
	err := r.DB.QueryRow(ctx, q, nID).Scan(&market.ID, &market.Title, &market.Description, &market.CreatedAt,
		&market.OwnersRoom, &market.OwnersPhone, &market.OwnerFirstName, &market.OwnerLastName)
	return market, err
}

func (r *Repo) CreateMarket(ctx context.Context, market domain.MarketDTO, uID int64) (int64, error) {
	q := `
insert into market (title, description, user_id, created_at) values ($1, $2, $3, now()) returning id`
	var id int64
	err := r.DB.QueryRow(ctx, q, market.Title, market.Description, uID).Scan(&id)
	return id, err
}

func (r *Repo) UpdateMarket(ctx context.Context, market domain.MarketDTO, nID int64) error {
	q := `
update market set title = coalesce($1, title), 
                description = coalesce($2, description), 
                created_at = now() 
            where id = $3`
	_, err := r.DB.Exec(ctx, q, market.Title, market.Description, nID)
	return err
}

func (r *Repo) DeleteMarket(ctx context.Context, nID int64) error {
	q := `
delete from market where id = $1`
	_, err := r.DB.Exec(ctx, q, nID)
	return err
}
