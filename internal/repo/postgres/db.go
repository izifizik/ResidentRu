package postgresql

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4/pgxpool"
	"resident_ru/internal/config"
)

// Repo - для флекса с бд ( обычно тут интерфейсы отдельных сущностей но тут то зачем)))) )
type Repo struct {
	DB *pgxpool.Pool
}

// NewRepo - комментарии излишни
func NewRepo(db *pgxpool.Pool) *Repo {
	return &Repo{
		DB: db,
	}
}

// CreateDatabasePoolConnections - по стринге коннект до базы данных
func CreateDatabasePoolConnections(cfg *config.DBConfig) (*pgxpool.Pool, error) {
	pool, err := pgxpool.Connect(context.Background(), cfg.DBString)
	if err != nil {
		return nil, errors.New("err init connection for new pool")
	}
	return pool, nil
}
