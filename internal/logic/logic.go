package logic

import (
	"resident_ru/internal/config"
	"resident_ru/internal/repo/postgres"
)

// Logic содержит все для доступа к данным (services, где происходит основная логика) Роуты принимают данные -> logic обрабатывает -> Repository отправляет в бд -> logic обрабатывает -> роут отдает ответ
type Logic struct {
	config *config.Config
	repo   *postgresql.Repo
}

// NewLogic - конструктор
func NewLogic(config *config.Config, repo *postgresql.Repo) *Logic {
	return &Logic{
		config: config,
		repo:   repo,
	}
}
