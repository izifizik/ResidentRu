package logic

import (
	"context"
	"resident_ru/internal/domain"
)

func (l *Logic) GetNews(ctx context.Context, title string) ([]domain.News, error) {
	return l.repo.GetNews(ctx, title)
}

func (l *Logic) GetNewsByID(ctx context.Context, nID int64) (domain.News, error) {
	return l.repo.GetNewsByID(ctx, nID)
}

func (l *Logic) CreateNews(ctx context.Context, news domain.NewsDTO) (int64, error) {
	return l.repo.CreateNews(ctx, news)
}

func (l *Logic) UpdateNews(ctx context.Context, news domain.NewsDTO, nID int64) error {
	return l.repo.UpdateNews(ctx, news, nID)
}

func (l *Logic) DeleteNews(ctx context.Context, nID int64) error {
	return l.repo.DeleteNews(ctx, nID)
}
