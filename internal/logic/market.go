package logic

import (
	"context"
	"resident_ru/internal/domain"
)

func (l *Logic) GetMarkets(ctx context.Context, title string) ([]domain.Market, error) {
	return l.repo.GetMarkets(ctx, title)
}

func (l *Logic) GetMarketByID(ctx context.Context, mID int64) (domain.Market, error) {
	return l.repo.GetMarketByID(ctx, mID)
}

func (l *Logic) CreateMarket(ctx context.Context, market domain.MarketDTO, uID int64) (int64, error) {
	return l.repo.CreateMarket(ctx, market, uID)
}

func (l *Logic) UpdateMarket(ctx context.Context, market domain.MarketDTO, mID int64) error {
	return l.repo.UpdateMarket(ctx, market, mID)
}

func (l *Logic) DeleteMarket(ctx context.Context, mID int64) error {
	return l.repo.DeleteMarket(ctx, mID)
}
