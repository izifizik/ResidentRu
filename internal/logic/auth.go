package logic

import (
	"context"
	"errors"
	"golang.org/x/crypto/bcrypt"
	"log"
	"resident_ru/internal/domain"
)

func (l *Logic) Login(ctx context.Context, a domain.AuthDTO) (domain.User, error) {
	hPass, err := l.repo.GetHashPasswordByLogin(ctx, a.Login)
	if err != nil {
		return domain.User{}, err
	}

	if !comparePasswords(hPass, a.Password) {
		return domain.User{}, errors.New("bad request")
	}

	return l.repo.GetUserByLogin(ctx, a.Login)
}

func (l *Logic) Registration(ctx context.Context, a domain.AuthDTO) (domain.User, error) {
	a.Password = hashAndSalt([]byte(a.Password))

	id, err := l.repo.CreateUser(ctx, a)
	if err != nil {
		return domain.User{}, err
	}
	return l.repo.GetUserByUserID(ctx, id)
}

func hashAndSalt(pwd []byte) string {
	hash, err := bcrypt.GenerateFromPassword(pwd, 4)
	if err != nil {
		log.Println(err)
	}

	return string(hash)
}

func comparePasswords(hashedPwd string, plainPwd string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPwd), []byte(plainPwd))
	if err != nil {
		return false
	}

	return true
}
