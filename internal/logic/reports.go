package logic

import (
	"context"
	"resident_ru/internal/domain"
)

func (l *Logic) GetReports(ctx context.Context, uID int64, isAdmin bool, title string) ([]domain.Reports, error) {
	return l.repo.GetReports(ctx, uID, isAdmin, title)
}

func (l *Logic) GetReportByID(ctx context.Context, rID int64) (domain.ReportedMessages, error) {
	return l.repo.GetReportByID(ctx, rID)
}

func (l *Logic) CreateMessage(ctx context.Context, message string, rID, uID int64) error {
	return l.repo.CreateMessage(ctx, message, rID, uID)
}

func (l *Logic) EditMessage(ctx context.Context, m domain.MessageDTO, mID int64) error {
	return l.repo.EditMessage(ctx, m, mID)
}

func (l *Logic) DeleteMessage(ctx context.Context, mID int64) error {
	return l.repo.DeleteMessage(ctx, mID)
}

func (l *Logic) CreateReport(ctx context.Context, report domain.ReportDTO, uID int64) (int64, error) {
	return l.repo.CreateReport(ctx, report, uID)
}

func (l *Logic) UpdateReport(ctx context.Context, report domain.ReportDTO, rID int64) error {
	return l.repo.UpdateReport(ctx, report, rID)
}

func (l *Logic) DeleteReport(ctx context.Context, rID int64) error {
	return l.repo.DeleteReport(ctx, rID)
}
