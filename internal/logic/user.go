package logic

import (
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"resident_ru/internal/domain"
)

func (l *Logic) GetUserByContext(c *gin.Context) (domain.User, error) {
	uInfo, ok := c.Get("user")
	if !ok {
		return domain.User{}, errors.New("user не найден в ctx")
	}
	return uInfo.(domain.User), nil
}

func (l *Logic) GetUserByUserID(ctx context.Context, uID int64) (domain.User, error) {
	return l.repo.GetUserByUserID(ctx, uID)
}
