package domain

import "time"

type News struct {
	ID          int64     `json:"id"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at"`
}

type NewsDTO struct {
	Title       *string `json:"title"`
	Description *string `json:"description"`
}
