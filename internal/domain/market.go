package domain

import "time"

type Market struct {
	ID             int64     `json:"id"`
	OwnersRoom     int64     `json:"owners_room"`
	OwnersPhone    int64     `json:"owners_phone"`
	Title          string    `json:"title"`
	Description    string    `json:"description"`
	OwnerLastName  string    `json:"owner_last_name"`
	OwnerFirstName string    `json:"owner_first_name"`
	CreatedAt      time.Time `json:"created_at"`
}

type MarketDTO struct {
	Title       *string `json:"title"`
	Description *string `json:"description"`
}
