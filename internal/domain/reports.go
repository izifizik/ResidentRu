package domain

import "time"

type Reports struct {
	ID          int64     `json:"id"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at"`
}

type ReportDTO struct {
	Title       *string `json:"title"`
	Description *string `json:"description"`
}

type ReportedMessages struct {
	Title       string     `json:"title"`
	Description string     `json:"description"`
	Messages    []Messages `json:"messages"`
}

type Messages struct {
	ID            int64  `json:"id"`
	UserID        int64  `json:"user_id"`
	Message       string `json:"message"`
	UserFirstName string `json:"user_first_name"`
	UserLastName  string `json:"user_last_name"`
}

type MessageDTO struct {
	Message string `json:"message"`
}
