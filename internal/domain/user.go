package domain

type User struct {
	ID        int64  `json:"id"`
	Room      int64  `json:"room"`
	Phone     int64  `json:"phone"`
	Login     string `json:"login"`
	LastName  string `json:"last_name"`
	FirstName string `json:"first_name"`
	IsAdmin   bool   `json:"is_admin"`
}

type AuthDTO struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}
