package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"net/http"
	"resident_ru/internal/domain"
	"strconv"
)

func (a *App) SetReportsRoutes(v1 *gin.RouterGroup) {
	reports := v1.Group("", a.auth)
	reports.GET("/reports", a.GetReports)
	reports.POST("/reports", a.CreateReport)

	reports.GET("/reports/:id", a.GetReportByID)
	reports.PUT("/reports/:id", a.UpdateReportByID)
	reports.DELETE("/reports/:id", a.DeleteReportByID)

	reports.POST("/reports/:id/messages", a.CreateMessage)
	reports.PUT("/reports/messages/:id", a.EditMessage)
	reports.DELETE("/reports/messages/:id", a.DeleteMessage)
}

func (a *App) GetReports(c *gin.Context) {
	title := c.Query("search")
	uInfo, err := a.getUserFromCTX(c)
	if err != nil {
		if err != nil {
			log.Error().Err(err).Msg("error with get user info")
			c.JSON(http.StatusInternalServerError, ErrorResponse{Message: err.Error()})
			return
		}
	}
	reports, err := a.logic.GetReports(c, uInfo.ID, uInfo.IsAdmin, title)
	if err != nil {
		log.Error().Err(err).Msg("error with get reports")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with get list of reports"})
		return
	}
	c.JSON(http.StatusOK, reports)
}

func (a *App) CreateReport(c *gin.Context) {
	uInfo, err := a.getUserFromCTX(c)
	if err != nil {
		if err != nil {
			log.Error().Err(err).Msg("error with get user info")
			c.JSON(http.StatusInternalServerError, ErrorResponse{Message: err.Error()})
			return
		}
	}
	var report domain.ReportDTO
	err = c.ShouldBind(&report)
	if err != nil {
		log.Error().Err(err).Msg("required fields is empty")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "required fields is empty"})
		return
	}
	id, err := a.logic.CreateReport(c, report, uInfo.ID)
	if err != nil {
		log.Error().Err(err).Msg("error with create report")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with create report"})
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"id": id,
	})
}

func (a *App) CreateMessage(c *gin.Context) {
	uInfo, err := a.getUserFromCTX(c)
	if err != nil {
		if err != nil {
			log.Error().Err(err).Msg("error with get user info")
			c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with create report message"})
			return
		}
	}
	rID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("invalid id param")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "invalid id param"})
		return
	}
	var m domain.MessageDTO
	err = c.ShouldBind(&m)
	if err != nil {
		log.Error().Err(err).Msg("required fields is empty")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "required fields is empty"})
		return
	}
	err = a.logic.CreateMessage(c, m.Message, rID, uInfo.ID)
	if err != nil {
		log.Error().Err(err).Msg("error with create report message")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with create report message"})
		return
	}

	c.Status(http.StatusOK)
}

func (a *App) EditMessage(c *gin.Context) {
	mID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("error with get message_id. invalid message_id value")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "invalid message_id value"})
		return
	}
	var m domain.MessageDTO
	err = c.ShouldBind(&m)
	if err != nil {
		log.Error().Err(err).Msg("required fields is empty")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "required fields is empty"})
		return
	}
	err = a.logic.EditMessage(c, m, mID)
	if err != nil {
		log.Error().Err(err).Msg("error with get report by id")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with get report by id"})
		return
	}
	c.Status(http.StatusOK)
}

func (a *App) DeleteMessage(c *gin.Context) {
	mID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("error with get message_id. invalid message_id value")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "invalid message_id value"})
		return
	}
	err = a.logic.DeleteMessage(c, mID)
	if err != nil {
		log.Error().Err(err).Msg("error with get report by id")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with get report by id"})
		return
	}
	c.Status(http.StatusOK)
}

func (a *App) GetReportByID(c *gin.Context) {
	rID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("error with get report_id. invalid report_id value")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "invalid report_id value"})
		return
	}
	report, err := a.logic.GetReportByID(c, rID)
	if err != nil {
		log.Error().Err(err).Msg("error with get report by id")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with get report by id"})
		return
	}
	c.JSON(http.StatusOK, report)
}

func (a *App) UpdateReportByID(c *gin.Context) {
	rID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("error with get report_id. invalid report_id value")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "invalid report_id value"})
		return
	}
	var report domain.ReportDTO
	err = c.ShouldBind(&report)
	if err != nil {
		log.Error().Err(err).Msg("required fields is empty")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "required fields is empty"})
		return
	}
	err = a.logic.UpdateReport(c, report, rID)
	if err != nil {
		log.Error().Err(err).Msg("error with update report")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with update report"})
		return
	}
	c.Status(http.StatusOK)
}

func (a *App) DeleteReportByID(c *gin.Context) {
	rID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("error with get report_id. invalid report_id value")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "invalid report_id value"})
		return
	}
	err = a.logic.DeleteReport(c, rID)
	if err != nil {
		log.Error().Err(err).Msg("error with update report")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with update report"})
		return
	}
	c.Status(http.StatusOK)
}
