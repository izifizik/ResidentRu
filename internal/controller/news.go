package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"net/http"
	"resident_ru/internal/domain"
	"strconv"
)

func (a *App) SetNewsRoutes(v1 *gin.RouterGroup) {
	news := v1.Group("", a.auth)
	v1.GET("/news", a.GetNews)
	news.POST("/news", a.CreateNews)

	v1.GET("/news/:id", a.GetNewsByID)
	news.PUT("/news/:id", a.UpdateNewsByID)
	news.DELETE("/news/:id", a.DeleteNewsByID)
}

func (a *App) GetNews(c *gin.Context) {
	title := c.Query("search")
	news, err := a.logic.GetNews(c, title)
	if err != nil {
		log.Error().Err(err).Msg("error with get news")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with get list of news"})
		return
	}
	c.JSON(http.StatusOK, news)
}

func (a *App) CreateNews(c *gin.Context) {
	var news domain.NewsDTO
	err := c.ShouldBind(&news)
	if err != nil {
		log.Error().Err(err).Msg("required fields is empty")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "required fields is empty"})
		return
	}
	id, err := a.logic.CreateNews(c, news)
	if err != nil {
		log.Error().Err(err).Msg("error with create news")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with create news"})
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"id": id,
	})
}

func (a *App) GetNewsByID(c *gin.Context) {
	nID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("error with get news_id. invalid news_id value")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "invalid news_id value"})
		return
	}
	news, err := a.logic.GetNewsByID(c, nID)
	if err != nil {
		log.Error().Err(err).Msg("error with get news by id")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with get news by id"})
		return
	}
	c.JSON(http.StatusOK, news)
}

func (a *App) UpdateNewsByID(c *gin.Context) {
	nID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("error with get news_id. invalid news_id value")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "invalid news_id value"})
		return
	}
	var news domain.NewsDTO
	err = c.ShouldBind(&news)
	if err != nil {
		log.Error().Err(err).Msg("required fields is empty")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "required fields is empty"})
		return
	}
	err = a.logic.UpdateNews(c, news, nID)
	if err != nil {
		log.Error().Err(err).Msg("error with update news")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with update news"})
		return
	}
	c.Status(http.StatusOK)
}

func (a *App) DeleteNewsByID(c *gin.Context) {
	nID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("error with get news_id. invalid news_id value")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "invalid news_id value"})
		return
	}
	err = a.logic.DeleteNews(c, nID)
	if err != nil {
		log.Error().Err(err).Msg("error with update news")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with update news"})
		return
	}
	c.Status(http.StatusOK)
}
