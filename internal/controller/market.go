package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"net/http"
	"resident_ru/internal/domain"
	"strconv"
)

func (a *App) SetMarketRoutes(v1 *gin.RouterGroup) {
	v1.GET("/market", a.GetMarkets)
	v1.GET("/market/:id", a.GetMarketByID)

	market := v1.Group("", a.auth)
	market.POST("/market", a.CreateMarket)
	market.PUT("/market/:id", a.UpdateMarketByID)
	market.DELETE("/market/:id", a.DeleteMarketByID)
}

func (a *App) GetMarkets(c *gin.Context) {
	title := c.Query("search")
	markets, err := a.logic.GetMarkets(c, title)
	if err != nil {
		log.Error().Err(err).Msg("error with get markets")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with get list of markets"})
		return
	}
	c.JSON(http.StatusOK, markets)
}

func (a *App) CreateMarket(c *gin.Context) {
	uInfo, err := a.getUserFromCTX(c)
	if err != nil {
		log.Error().Err(err).Msg("error with create market")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: err.Error()})
		return
	}
	var market domain.MarketDTO
	err = c.ShouldBind(&market)
	if err != nil {
		log.Error().Err(err).Msg("required fields is empty")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "required fields is empty"})
		return
	}
	id, err := a.logic.CreateMarket(c, market, uInfo.ID)
	if err != nil {
		log.Error().Err(err).Msg("error with create market")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with create market"})
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"id": id,
	})
}

func (a *App) GetMarketByID(c *gin.Context) {
	nID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("error with get market_id. invalid market_id value")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "invalid market_id value"})
		return
	}
	market, err := a.logic.GetMarketByID(c, nID)
	if err != nil {
		log.Error().Err(err).Msg("error with get market by id")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with get market by id"})
		return
	}
	c.JSON(http.StatusOK, market)
}

func (a *App) UpdateMarketByID(c *gin.Context) {
	nID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("error with get market_id. invalid market_id value")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "invalid market_id value"})
		return
	}
	var market domain.MarketDTO
	err = c.ShouldBind(&market)
	if err != nil {
		log.Error().Err(err).Msg("required fields is empty")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "required fields is empty"})
		return
	}
	err = a.logic.UpdateMarket(c, market, nID)
	if err != nil {
		log.Error().Err(err).Msg("error with update market")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with update market"})
		return
	}
	c.Status(http.StatusOK)
}

func (a *App) DeleteMarketByID(c *gin.Context) {
	nID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("error with get market_id. invalid market_id value")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "invalid market_id value"})
		return
	}
	err = a.logic.DeleteMarket(c, nID)
	if err != nil {
		log.Error().Err(err).Msg("error with update market")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with update market"})
		return
	}
	c.Status(http.StatusOK)
}
