package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"net/http"
	"resident_ru/internal/domain"
	"strings"
)

func (a *App) SetAuthRoutes(v1 *gin.RouterGroup) {
	v1.POST("/login", a.Login)
	v1.POST("/registration", a.Registration)
}

func (a *App) Login(c *gin.Context) {
	var auth domain.AuthDTO
	err := c.ShouldBindJSON(&auth)
	if err != nil {
		log.Error().Err(err).Msg("error with login. Required fields is empty")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "error with login. Required fields is empty"})
		return
	}
	user, err := a.logic.Login(c, auth)
	if err != nil {
		if strings.Contains(err.Error(), "bad request") {
			log.Error().Msg("password is incorrect")
			c.JSON(http.StatusBadRequest, ErrorResponse{Message: "password is incorrect"})
			return
		}
		log.Error().Err(err).Msg("error with login")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with login"})
		return
	}

	c.JSON(http.StatusOK, user)
}

func (a *App) Registration(c *gin.Context) {
	var auth domain.AuthDTO
	err := c.ShouldBind(&auth)
	if err != nil {
		log.Error().Err(err).Msg("error with registration. Required fields is empty")
		c.JSON(http.StatusBadRequest, ErrorResponse{Message: "error with registration. Required fields is empty"})
		return
	}
	user, err := a.logic.Registration(c, auth)
	if err != nil {
		log.Error().Err(err).Msg("error with registration")
		c.JSON(http.StatusInternalServerError, ErrorResponse{Message: "error with registration"})
		return
	}

	c.JSON(http.StatusOK, user)
}
