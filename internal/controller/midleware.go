package controller

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"net/http"
	"resident_ru/internal/domain"
	"strconv"
)

func (a *App) auth(c *gin.Context) {
	userID, err := strconv.ParseInt(c.Request.Header.Get("Authorization"), 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("error with auth with \"" + c.Request.Header.Get("Authorization") + "\" in header Authorization")
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"message": "error with auth with \"" + c.Request.Header.Get("Authorization") + "\" in header Authorization",
		})
		return
	}
	user, err := a.logic.GetUserByUserID(c, userID)
	if err != nil {
		log.Error().Err(err).Msg("error with get user info")
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"message": "error with get user info",
		})
		return
	}
	c.Set("user", user)

	c.Next()
}

func (a *App) getUserFromCTX(c *gin.Context) (domain.User, error) {
	user, ok := c.Get("user")
	if !ok {
		return domain.User{}, errors.New("error with get user from ctx")
	}
	return user.(domain.User), nil
}
