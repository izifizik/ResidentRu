package controller

import (
	"context"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"net/http"
	"os"
	"os/signal"
	"resident_ru/internal/config"
	"resident_ru/internal/logic"
	"strconv"
	"syscall"
	"time"
)

// ErrorResponse типовой ответ с ошибкой
type ErrorResponse struct {
	Message string `json:"message"`
}

// ListData содержит список элементов ответа
type ListData struct {
	Items interface{} `json:"items"`
	Range ListRange   `json:"range"`
}

// ListRange описывает пейджер в ответе
type ListRange struct {
	Count  int64 `json:"count"`
	Limit  int64 `json:"limit"`
	Offset int64 `json:"offset"`
}

// App основная структура для приложения
type App struct {
	router *gin.Engine
	config *config.Config
	logic  *logic.Logic
}

func (a *App) setV1Routes(router *gin.RouterGroup) {
	v1 := router.Group("/v1")
	a.SetAuthRoutes(v1)
	a.SetMarketRoutes(v1)
	a.SetReportsRoutes(v1)
	a.SetNewsRoutes(v1)
}

func NewApp(config *config.Config, logic *logic.Logic) *App {
	if os.Getenv("DEBUG") == "true" {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}

	return &App{
		router: gin.Default(),
		config: config,
		logic:  logic,
	}
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Accept, Origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func (a *App) StartServe() {
	a.router.Use(CORSMiddleware())

	a.setV1Routes(a.router.Group("/api"))
	a.router.Use(static.Serve("/", static.LocalFile("./build", true)))

	server := &http.Server{
		Addr:           ":" + a.config.App.Port, // ":" + a.config.App.Port - под капотом localhost: + port from cfg
		Handler:        a.router,
		ReadTimeout:    time.Second * 15,
		WriteTimeout:   time.Second * 15,
		MaxHeaderBytes: 1 << 20,
	}

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	go func() {
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal().Err(err).Msg("Service running error")
		}
	}()

	<-ctx.Done()
	log.Info().Msg("Server stopping...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		log.Error().Err(err).Msg("Server forced to shutdown")
	}
	log.Info().Msg("Server stopped")
}

// GetFilters готовит параметры фильтрации запроса
func (a *App) GetFilters(c *gin.Context) (limit, offset int64) {
	var err error

	limit, err = strconv.ParseInt(c.Query("limit"), 10, 64)
	if err != nil || limit <= 0 {
		limit = 1000
	}
	offset, err = strconv.ParseInt(c.Query("offset"), 10, 64)
	if err != nil || offset < 0 {
		offset = 0
	}
	return
}
